from logging import Logger
import os
import re
from nyabs_params import NyabsParameter, NyabsCommandParameters
from nyabs_command import NyabsCommand

class ReferenceOrderCommand(NyabsCommand):

    class Item:

        def __init__(self, arg: NyabsParameter) -> None:
            self._arg = arg
            self._name = re.search(r"([\w\.]*).csproj$", arg.value).group(1)
            self._refs = set[str]()
            sfolder = arg.args.get("folder", "")
            with open(os.path.join(sfolder, arg.value), encoding="utf-8", mode="r") as f:
                for line in f:
                    m = re.search(r"<Reference Include=\"([\w\.]*)", line)
                    if (m == None):
                        if ("<ProjectReference" in line):
                            m = re.search(r"([\w\.]*)\.csproj", line)
                    if (m != None):
                        self._refs.add(m.group(1))

        def contains(self, ref: str) -> bool:
            return ref in self._refs

        @property
        def name(self) -> str:
            return self._name   

        @property
        def argument(self) -> NyabsParameter:
            return self._arg 

        @property
        def count(self) -> int:
            return len(self._refs)

    def __init__(self) -> None:
        super().__init__(ordered = True, accumulate = True)

    def prepare(self, args: list[NyabsParameter], log: Logger) -> list[NyabsCommandParameters]:
        items = list[ReferenceOrderCommand.Item]()        
        for arg in args:
           items.append(ReferenceOrderCommand.Item(arg))
        items.sort(key = lambda item: item.count, reverse=True)
        refs = set[str]()
        cnt = 0
        for item in items:
            refs.add(item.name)
            cnt += 1
        rslt = list[NyabsCommandParameters]()
        while (cnt > 0):            
            narg = NyabsCommandParameters("build")
            icnt = cnt
            rrefs = list[str]()
            while (icnt > 0):
                icnt -= 1
                item = items[icnt]
                finded = True
                for ref in refs:
                    if (item.contains(ref)):
                        finded = False
                        break
                if (finded):
                    del items[icnt]
                    rrefs.append(item.name)                    
                    narg.items.append(item.argument)
                    log.debug("Finded refs " + item.name)
                    cnt -= 1   
            for ref in rrefs:
                refs.remove(ref)            
            if (len(narg.items) == 0):
                log.error("Cyclical reference!")
                return None
            rslt.append(narg)
        return rslt

    async def execute(self, args: list[NyabsParameter], log: Logger) -> bool:
        return True