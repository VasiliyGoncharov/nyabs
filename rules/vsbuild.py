
import asyncio
from logging import Logger
import os
from nyabs_params import NyabsParameter, NyabsCommandParameters
from nyabs_command import NyabsCommand

class VSBuildCommand(NyabsCommand):

    def __init__(self) -> None:
        super().__init__()

    def prepare(self, args: list[NyabsParameter], log: Logger) -> list[NyabsCommandParameters]:
        nargs = NyabsCommandParameters(None)
        for arg in args:
            nargs.items.append(NyabsParameter(arg.value.replace(".csproj", ".dll")))
        rslt = list[NyabsCommandParameters]()
        rslt.append(nargs)
        return rslt       

    async def execute(self, args: list[NyabsParameter], log: Logger) -> bool:
        for arg in args:
            if (arg.args == None):
                log.error("Parameter %s not contains attributes", arg.value)
                return False
            scofiguration = arg.args.get("cofiguration", None)
            splatform = arg.args.get("platform", None)
            sfolder = arg.args.get("folder", None)
            if (sfolder == None):
                log.error("Parameter %s not contains folder attribute", arg.value)
                return False
            if ((scofiguration == None) or (splatform == None)):
                log.error("Parameter %s not contains configuration or platform attribute", arg.value)
                return False
            log.info("%s building started.", arg.value)                   
            process = await asyncio.create_subprocess_exec('C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\msbuild.exe', 
                os.path.join(sfolder, arg.value), '/t:Build', '/p:Configuration=' + scofiguration, '/p:Platform=' + splatform, '/nologo', '/m:1', '-clp:ErrorsOnly', 
                stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)
            stdout, stderr = await process.communicate()
            if (process.returncode != 0):
                log.error(str(stdout, "cp866"))    
                log.error(str(stderr, "cp866"))                              
                return False
            log.info("%s building complete.", arg.value)                       
        return True       


        
