from abc import abstractmethod
from nyabs_params import NyabsParameter, NyabsCommandParameters
from logging import Logger

class NyabsCommand:

    def __init__(self, ordered: bool = False, accumulate: bool = False) -> None:
        self._accumulate = accumulate
        self._ordered = ordered

    @property
    def accumulate(self) -> bool:
        return self._accumulate

    @property
    def ordered(self) -> bool:
        return self._ordered        
    
    @abstractmethod
    def prepare(self, args: list[NyabsParameter], log: Logger) -> list[NyabsCommandParameters]:
        raise NotImplementedError

    @abstractmethod
    async def execute(self, args: list[NyabsParameter], log: Logger) -> bool:
        raise NotImplementedError    