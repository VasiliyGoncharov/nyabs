import asyncio
from logging import Logger
import logging
import os
from nyabs import Nyabs
from nyabs_command import NyabsCommand
from nyabs_params import NyabsCommandParameters, NyabsParameter

class SourceCommand(NyabsCommand):

    def __init__(self, sources: str) -> None:
        super().__init__(False, False)
        self._sources = sources
    
    def prepare(self, args: list[NyabsParameter], log: Logger) -> list[NyabsCommandParameters]:
        return None

    async def execute(self, args: list[NyabsParameter], log: Logger) -> bool:
        for arg in args:
            bname = os.path.basename(arg.value)
            not_found = True
            for root, dirs, files in os.walk(self._sources):
                if bname in files:
                    process = await asyncio.create_subprocess_shell("copy /y \"" + arg.value + "\" \"" + os.path.join(root, bname) + "\"",  
                        stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)
                    stdout, stderr = await process.communicate()
                    if (process.returncode != 0):
                        log.error(str(stdout, "cp866"))    
                        log.error(str(stderr, "cp866"))                              
                        return False
                    log.info("%s copied succesful to %s.", arg.value, os.path.join(root, bname))
                    not_found = False
            if (not_found):
                log.warning("File %s not found in source dir", bname)
        # Do nothing
        return True
    
class SvgCommand(NyabsCommand):

    def __init__(self, svg: str) -> None:
        super().__init__(True, True)
        self._svg = svg
    
    def prepare(self, args: list[NyabsParameter], log: Logger) -> list[NyabsCommandParameters]:

        s1_0 = { "scale": 1.0 }
        s1_5 = { "scale": 1.5 }
        s2_0 = { "scale": 2.0 }
        s2_5 = { "scale": 2.5 }
        s3_0 = { "scale": 3.0 }

        cmd = NyabsCommandParameters("inkscape")
        for arg in args:
            cmd.items.append(NyabsParameter(arg.value, s1_0))
            cmd.items.append(NyabsParameter(arg.value, s1_5))
            cmd.items.append(NyabsParameter(arg.value, s2_0))
            cmd.items.append(NyabsParameter(arg.value, s2_5))
            cmd.items.append(NyabsParameter(arg.value, s3_0))

        rslt = list[NyabsCommandParameters]()
        rslt.append(cmd)
        return rslt        

    async def execute(self, args: list[NyabsParameter], log: Logger) -> bool:
        for arg in args:
            bname = os.path.basename(arg.value)
            process = await asyncio.create_subprocess_shell("copy /y \"" + arg.value + "\" \"" + os.path.join(self._svg, bname) + "\"",  
                stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)
            stdout, stderr = await process.communicate()
            if (process.returncode != 0):
                log.error(str(stdout, "cp866"))    
                log.error(str(stderr, "cp866"))                              
                return False
            log.info("%s copied succesful.", arg.value)
        return True

class InkscapeCommand(NyabsCommand):
        
    def __init__(self, inkscape: str, png: str) -> None:
        super().__init__(False, False)
        self._inkscape = inkscape
        self._png = png
    
    def prepare(self, args: list[NyabsParameter], log: Logger) -> list[NyabsCommandParameters]:
        return None

    async def execute(self, args: list[NyabsParameter], log: Logger) -> bool:
        for arg in args:
            size = 16
            index = arg.value.rindex("_")
            if (index > 0):
                ssize = arg.value[index + 1:].replace("dp.svg", "")
                if (ssize.isdigit()):
                    size = int(ssize)
            scale = arg.args.get("scale", 1.0)
            bname = os.path.splitext(os.path.basename(arg.value))[0]
            png_name = os.path.join(self._png, bname + "_" + '%g' % (scale) + "x.png")
            process = await asyncio.create_subprocess_exec(self._inkscape, "--export-filename=" + png_name, "--export-background-opacity=0", "--export-height=" + str(round(size * scale)),
                arg.value, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)
            stdout, stderr = await process.communicate()
            if (process.returncode != 0):
                log.error(str(stdout, "cp866"))    
                log.error(str(stderr, "cp866"))                              
                return False
            log.info("%s export succesful.", png_name)
        # Do nothing
        return True

logging.basicConfig(level=logging.INFO)

print("nyabs started")

g = Nyabs(8)

folder_path = "C:\\Temp\\Voldemar"
inkscape_path = "C:\\Program Files\\Inkscape\\bin\\inkscape.com"
source_path = "C:\\Topomatic\\Projects"
svg_path = source_path + "\\Sources\\Common\\Icons\\svg"
png_path = source_path + "\\Sources\\Common\\Icons\\png"

g.append_command("replace", SourceCommand(source_path))
g.append_command("svg", SvgCommand(svg_path))
g.append_command("inkscape", InkscapeCommand(inkscape_path, png_path))

scmd = NyabsCommandParameters("svg")
rcmd = NyabsCommandParameters("replace")


for fname in os.listdir(folder_path):
    if (fname.isascii()):
        path = os.path.join(folder_path, fname)
        if (os.path.isfile(path)):
            stext = os.path.splitext(fname)
            if ("svg" in stext[1]):
                scmd.items.append(NyabsParameter(path))
            else:
                rcmd.items.append(NyabsParameter(path))
    else:
        logging.getLogger().warning("File %s contains non ASCII symbols", fname)

g.init_parameters(scmd)
g.init_parameters(rcmd)

g.run()
