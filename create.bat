@echo off
timeout /t 2 >NUL
if "%2" == "" (
    echo %1 > ./%1.tmp
) else (
    if EXIST ./%2.tmp (
        echo %1> ./%1.tmp
        echo %2>> ./%1.tmp
        exit /B 0
    )
    exit /B 1
)
exit /B 0

