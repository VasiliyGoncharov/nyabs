from abc import abstractmethod
import asyncio
from logging import Logger
import logging
from nyabs_params import NyabsParameter, NyabsCommandParameters
from nyabs_command import NyabsCommand

class Nyabs:

    class _Node:

        def __init__(self, rule: str) -> None:
            self._rule = rule
            self._nodes = list[Nyabs._Node]()  

        @property
        def rule(self) -> str:
            return self._rule

        def get_nodes(self) -> list['Nyabs._Node']:
            return self._nodes

        @abstractmethod
        def get_args(self) -> list[NyabsParameter]:
            raise NotImplementedError            

        @abstractmethod
        def is_complete(self) -> bool:
            raise NotImplementedError

    class _SingleNode(_Node):

        def __init__(self, rule: str, argument: NyabsParameter) -> None:
            super().__init__(rule)
            self._argument = argument

        def is_complete(self) -> bool:
            return True

        def get_args(self) -> list[NyabsParameter]:
            l = list[NyabsParameter]()
            l.append(self._argument)
            return l

    class _AccumulatedNode(_Node):

        def __init__(self, rule: str) -> None:
            super().__init__(rule)
            self._count = 0
            self._args = list[NyabsParameter]()

        def append(self, argument: NyabsParameter):
            self._args.append(argument)
            self._count += 1

        def get_args(self) -> list[NyabsParameter]:
            return self._args

        def is_complete(self) -> bool:
            self._count -= 1
            return self._count == 0

    class _OrderedNode(_Node):

        def __init__(self, count: int) -> None:
            super().__init__(None)
            self._count = count

        def is_complete(self) -> bool:
            self._count -= 1
            return self._count == 0

        def get_args(self) -> list[NyabsParameter]:
            return None

    def __init__(self, concurrency: int) -> None:
         self._commands = dict[str, NyabsCommand]()
         self._parameters = list[NyabsCommandParameters]()
         self._log = logging.getLogger()
         self._queue = asyncio.Queue[Nyabs._Node]()
         self._lock = asyncio.Lock()
         self._concurrency = concurrency
         self._broken = False
    
    def init_parameters(self, parameters: NyabsCommandParameters) -> None:
        self._parameters.append(parameters)  

    def append_command(self, command_id: str, command: NyabsCommand) -> None:
        self._commands[command_id] = command

    def _prepare_single(self, nodes: list['Nyabs._Node'], accumulate: dict[str, 'Nyabs._AccumulatedNode'], values: list[NyabsCommandParameters]) -> bool:
        for value in values:
            for argument in value.items:
                inner_node = Nyabs._SingleNode(value.command_id, argument)
                nodes.append(inner_node)
                if (not self._recurce(inner_node, accumulate)):
                    return False     
        return True        
                
    def _prepare_ordered(self, nodes: list['Nyabs._Node'], accumulate: dict[str, 'Nyabs._AccumulatedNode'], values: list[NyabsCommandParameters]) -> bool:
        buffer_nodes = nodes
        for value in values:
            ord_node = Nyabs._OrderedNode(len(value.items))
            for argument in value.items:
                inner_node = Nyabs._SingleNode(value.command_id, argument)
                inner_node.get_nodes().append(ord_node)
                buffer_nodes.append(inner_node)
                if (not self._recurce(inner_node, accumulate)):
                    return False                            
            buffer_nodes = ord_node.get_nodes()
        return True

    def _recurce(self, node: 'Nyabs._Node', accumulate: dict[str, 'Nyabs._AccumulatedNode']) -> bool:
        if (node.rule != None):
            command = self._commands.get(node.rule, None)
            if (command == None):
                self._log.error("Command %s not exists", node.rule)
                return False
            if (command.accumulate):
                acc_node = accumulate.get(node.rule, Nyabs._AccumulatedNode(node.rule))
                for argument in node.get_args():
                    acc_node.append(argument)
                accumulate[node.rule] = acc_node
                node.get_nodes().append(acc_node)
            else:
                values = command.prepare(node.get_args(), self._log)
                if (values == None):
                    self._log.debug("Command %s prepare no values", node.rule)
                else:
                    if (command.ordered):
                        if (not self._prepare_ordered(node.get_nodes(), accumulate, values)):
                            return False
                    else:
                        if (not self._prepare_single(node.get_nodes(), accumulate, values)):
                            return False            
        return True

    def _prepare(self, nodes: list['Nyabs._Node']) -> bool:
        nodes.clear()
        accumulate = dict[str, Nyabs._AccumulatedNode]()
        if (not self._prepare_single(nodes, accumulate, self._parameters)):
            return False
        while (True):
            tacc = dict[str, Nyabs._AccumulatedNode]()
            for key in accumulate:
                acc_node = accumulate[key]
                command = self._commands.get(acc_node.rule, None)
                if (command == None):
                    self._log.error("Command %s not exists", acc_node.rule)
                    return False 
                values = command.prepare(acc_node.get_args(), self._log)
                if (values == None):
                    self._log.debug("Command %s prepare no values", acc_node.rule)
                else:
                    if (command.ordered):
                        if (not self._prepare_ordered(acc_node.get_nodes(), tacc, values)):
                            return False
                    else:
                        if (not self._prepare_single(acc_node.get_nodes(), tacc, values)):
                            return False                        
            if (len(tacc) == 0):
                break
            accumulate = tacc
        return True

    async def _run_task(self) -> None:
        while True:
            node = await self._queue.get()
            try:
                rule = None
                complete = False
                async with self._lock:
                    if (self._broken):
                        self._log.info("Task break")
                        break
                    complete = node.is_complete()
                    if (complete and (node.rule != None)):
                        rule = self._commands.get(node.rule, None)
                done = True
                if (rule != None):
                    done = await rule.execute(node.get_args(), self._log)
                if (done):
                    if (complete):
                        for n in node.get_nodes():
                            await self._queue.put(n)
                else:
                    async with self._lock:
                        self._broken = True
            except BaseException as err:
                async with self._lock:
                    self._log.exception("Raised exception on running task, stopped", exc_info=1)
                    self._broken = True
                break
            finally:
                self._queue.task_done()
    
    async def _run_tasks(self) -> None:
        tasks = list()
        for i in range(self._concurrency):
            tasks.append(asyncio.ensure_future(self._run_task()))
        await self._queue.join()
        for task in tasks:
            if (not task.done()):
                task.cancel()   
        
    def run(self) -> bool:
        self._log.info("prepare graph...")        
        nodes = list[Nyabs._Node]()
        if (not self._prepare(nodes)):
            return False
        self._log.info("done.")
        self._log.info("running commands...")        
        loop = asyncio.new_event_loop()
        try:
            for node in nodes:
                self._queue.put_nowait(node)
            loop.run_until_complete(self._run_tasks())
        finally:
            loop.run_until_complete(loop.shutdown_asyncgens())
            loop.close()
        if (not self._broken):
            self._log.info("done.")
            return True
        self._log.info("failed.")
        return False