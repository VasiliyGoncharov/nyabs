
class NyabsParameter:

    def __init__(self, value: str, args: dict = None) -> None:
        self._value = value
        self._args = args

    @property
    def value(self) -> str:
        return self._value

    @property
    def args(self) -> dict:
        return self._args

class NyabsCommandParameters:

    def __init__(self, command_id: str) -> None:
        self._command_id = command_id
        self._items = list[NyabsParameter]()

    @property
    def command_id(self) -> str:
        return self._command_id    

    @property
    def items(self) -> list[NyabsParameter]:
        return self._items  

