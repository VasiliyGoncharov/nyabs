import logging
from nyabs import Nyabs
from nyabs_params import NyabsCommandParameters, NyabsParameter
from rules.referenceorder import ReferenceOrderCommand
from rules.vsbuild import VSBuildCommand
from tests import TestCreateCommand, TestAccumulateCommand, TestDependendCommand, TestZipIconsCommand

logging.basicConfig(level=logging.DEBUG)

print("nyabs started")

g = Nyabs(8)

#nargs1 = NyabsCommandParameters("create")
#nargs1.items.append(NyabsParameter("a"))
#nargs1.items.append(NyabsParameter("b"))
#nargs1.items.append(NyabsParameter("c"))
#g.init_parameters(nargs1)

#nargs2 = NyabsCommandParameters("depend")
#nargs2.items.append(NyabsParameter("d", { "dependent": "a" }))
#nargs2.items.append(NyabsParameter("f", { "dependent": "b" }))
#nargs2.items.append(NyabsParameter("e", { "dependent": "c" }))
#g.init_parameters(nargs1)

#g.append_command(TestGroupCommand())
#g.append_command("create", TestCreateCommand())
#g.append_command("depend", TestDependendCommand())
#g.append_command("accumulate", TestAccumulateCommand())


nargs1 = NyabsCommandParameters("reforder")
args = { "cofiguration": "Debug", "platform" : "AnyCpu", "folder": "C:\\Topomatic\\Projects\\Sources\\" }
nargs1.items.append(NyabsParameter('Modules\\Robur\\Robur.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Extentions\\ltg2xml\\ltg2xml\\ltg2xml.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\SharpZipLib\\ICSharpCode.SharpZLib\\ICSharpCode.SharpZipLib.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\PdfSharp\\PdfSharp-gdi\\PdfSharp-gdi.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Fluent.RibbonBinary\\Fluent.RibbonBinary\\Fluent.RibbonBinary.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Docking\\Fireball.Docking\\Fireball.Docking.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Scripting\\Topomatic.Scripting.IronPython\\Topomatic.Scripting.IronPython.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Graphics\\Topomatic.Graphics.OpenGL\\Topomatic.Graphics.OpenGL.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Stg\\Topomatic.Stg\\Topomatic.Stg.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Stg\\Topomatic.Stg.Thumbnail\\Topomatic.Stg.Thumbnail.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\ComponentModel\\Topomatic.ComponentModel\\Topomatic.ComponentModel.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\ApplicationEnvironment\\Topomatic.ApplicationEnvironment\\Topomatic.ApplicationEnvironment.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\FoundationClasses\\Topomatic.FoundationClasses\\Topomatic.FoundationClasses.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Cad\\Topomatic.Cad.Foundation\\Topomatic.Cad.Foundation.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Controls\\Topomatic.Controls\\Topomatic.Controls.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Smt\\Topomatic.Smt\\Topomatic.Smt.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Cad\\Topomatic.Cad.View\\Topomatic.Cad.View.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Codifier\\Topomatic.Codifier\\Topomatic.Codifier.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Libx\\Topomatic.Libx\\Topomatic.Libx.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\StandardsLibrary\\Topomatic.StandardsLibrary\\Topomatic.StandardsLibrary.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\GeometryGymIFC\\DLLProjects\\GeometryGymIFC\\GeometryGymIFC.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Dwg\\Topomatic.Dwg\\Topomatic.Dwg.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Visualization\\Topomatic.Visualization\\Topomatic.Visualization.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Visualization\\Topomatic.Visualization.Tools\\Topomatic.Visualization.Tools.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Lidar\\Topomatic.Lidar\\Topomatic.Lidar.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\cadastre\\Topomatic.Cadastre\\Topomatic.Cadastre.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Tables\\Topomatic.Tables\\Topomatic.Tables.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Dwg\\Topomatic.Dwg.Smt\\Topomatic.Dwg.Smt.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Crs\\Topomatic.Crs\\Topomatic.Crs.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Extentions\\Topomatic.Glg\\Topomatic.Glg.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Mockup\\Topomatic.Mockup\\Topomatic.Mockup.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg\\Topomatic.Alg.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Sfc\\Topomatic.Sfc\\Topomatic.Sfc.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Cartogramms\\Topomatic.Cartograms\\Topomatic.Cartograms.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Its\\Topomatic.Its\\Topomatic.Its.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Rsf\\Topomatic.Rsf\\Topomatic.Rsf.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Arrangements\\Topomatic.Arrangements\\Topomatic.Arrangements.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Opr\\Topomatic.Opr\\Topomatic.Opr.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Acax\\Topomatic.Acax.Export\\Topomatic.Acax.Export.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Straightening\\Topomatic.Alg.Straightening.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.LandAllotment\\Topomatic.Alg.LandAllotment.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Acax\\Topomatic.Acax.Export.Dwg\\Acax.Export.Dwg.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Acax\\Topomatic.Acax.Import.Dxf\\Topomatic.Acax.Import.Dxf.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Acax\\Topomatic.Acax.Import.Dwg\\Acax.Import.Dwg.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Model\\Topomatic.Alg.Model.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Extentions\\Topomatic.Glg.Model\\Topomatic.Glg.Model.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Rail\\Topomatic.Alg.Rail.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Ecs\\Topomatic.Ecs\\Topomatic.Ecs.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Turnouts\\Topomatic.Turnouts\\Topomatic.Turnouts.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Turnouts\\Topomatic.Turnouts.Model\\Topomatic.Turnouts.Model.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Road\\Topomatic.Alg.Road.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Road.PlanVisibility\\Topomatic.Alg.Road.PlanVisibility.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Survey\\Topomatic.Alg.Survey.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Dwg\\Topomatic.Dwg.Layer\\Topomatic.Dwg.Layer.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Acax\\Topomatic.Acax.Export.Pdf\\Topomatic.Acax.Export.Pdf.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Crs\\Topomatic.Crs.Runtime\\Topomatic.Crs.Runtime.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Dtm\\Topomatic.Dtm\\Topomatic.Dtm.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Sfc\\Topomatic.Sfc.Layer\\Topomatic.Sfc.Layer.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Planchet\\Topomatic.Planchet\\Topomatic.Planchet.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\PlanOptimizator\\PlanOptimizator\\Topomatic.PlnOpt.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\srv\\Topomatic.Srv\\Topomatic.Srv.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\srv\\Topomatic.Srv.Layer\\Topomatic.Srv.Layer.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Layer\\Topomatic.Alg.Layers.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Rsf\\Topomatic.Rsf.Layers\\Topomatic.Rsf.Layers.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Arrangements\\Topomatic.Arrangements.Layers\\Topomatic.Arrangements.Layers.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Extentions\\Topomatic.Glg.Layers\\Topomatic.Glg.Layers.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Straightening.Layers\\Topomatic.Alg.Straightening.Layers.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.LandAllotment.Layers\\Topomatic.Alg.LandAllotment.Layers.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Rail.Layers\\Topomatic.Alg.Rail.Layers.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Road.Layers\\Topomatic.Alg.Road.Layers.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Survey.Layers\\Topomatic.Alg.Survey.Layers.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Cartogramms\\Topomatic.Cartograms.Layers\\Topomatic.Cartograms.Layers.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Its\\Topomatic.Its.Layers\\Topomatic.Its.Layers.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Turnouts\\Topomatic.Turnouts.Layers\\Topomatic.Turnouts.Layers.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Ecs\\Topomatic.Ecs.Layers\\Topomatic.Ecs.Layers.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Road.Crossing\\Topomatic.Alg.Road.Crossing.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\RoadMarking\\Topomatic.RoadMarking\\Topomatic.RoadMarking.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\RoadSigns\\Topomatic.RoadSigns\\Topomatic.RoadSigns.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Road.PlanVisibility.Layers\\Topomatic.Alg.Road.PlanVisibility.Layers.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Extentions\\Topomatic.Proj\\Topomatic.Proj.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Ifc\\Topomatic.Ifc\\Topomatic.Ifc.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Ifc\\Topomatic.Ifc.Layer\\Topomatic.Ifc.Layer.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Pipes\\Topomatic.Pipes\\Topomatic.Pipes.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Sites\\Topomatic.Sites\\Topomatic.Sites.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\ApplicationPlatform\\Topomatic.ApplicationPlatform\\Topomatic.ApplicationPlatform.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Plt\\Topomatic.Plt\\Topomatic.Plt.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Dwg\\Topomatic.Dwg.UI\\Topomatic.Dwg.UI.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Pipes\\Topomatic.Pipes.Layers\\Topomatic.Pipes.Layers.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Sites\\Topomatic.Sites.Layer\\Topomatic.Sites.Layer.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Visualization\\Topomatic.Visualization.Runtime\\Topomatic.Visualization.Runtime.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Visualization\\Topomatic.Visualization.Libx\\Topomatic.Visualization.Libx.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Dtm\\Topomatic.Dtm.Layer\\Topomatic.Dtm.Layer.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Planchet\\Topomatic.Planchet.Runtime\\Topomatic.Planchet.Runtime.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Dtm\\Topomatic.Dtm.Core\\Topomatic.Dtm.Core.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Runtime\\Topomatic.Alg.Runtime.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Tables\\Topomatic.Tables.Export\\Topomatic.Tables.Export.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Tables\\Topomatic.Tables.Core\\Topomatic.Tables.Core.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Extentions\\Topomatic.Glg.Runtime\\Topomatic.Glg.Runtime.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Survey.Runtime\\Topomatic.Alg.Survey.Runtime.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Rail.Runtime\\Topomatic.Alg.Rail.Runtime.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Road.Runtime\\Topomatic.Alg.Road.Runtime.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Core\\Topomatic.Alg.Core.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Survey.Core\\Topomatic.Alg.Survey.Core.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Rail.Core\\Topomatic.Alg.Rail.Core.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Pipes\\Topomatic.Pipes.Runtime\\Topomatic.Pipes.Runtime.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Cartogramms\\Topomatic.Cartograms.Core\\Topomatic.Cartograms.Core.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Arrangements\\Topomatic.Arrangements.Core\\Topomatic.Arrangements.Core.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\srv\\Topomatic.Srv.Core\\Topomatic.Srv.Core.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Turnouts\\Topomatic.Turnouts.Core\\Topomatic.Turnouts.Core.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Ecs\\Topomatic.Ecs.Core\\Topomatic.Ecs.Core.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.LandAllotment.Core\\Topomatic.Alg.LandAllotment.Core.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Road.Core\\Topomatic.Alg.Road.Core.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Road.PlanVisibility.Core\\Topomatic.Alg.Road.PlanVisibility.Core.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Road.Crossing.Core\\Topomatic.Alg.Road.Crossing.Core.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Rsf\\Topomatic.Rsf.Core\\Topomatic.Rsf.Core.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Its\\Topomatic.Its.Core\\Topomatic.Its.Core.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\GenPlan\\Topomatic.GenPlan.Controller\\Topomatic.GenPlan.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\RoadSigns\\Topomatic.RoadSigns.Runtime\\Topomatic.RoadSigns.Runtime.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\RoadSigns\\Topomatic.RoadSigns.Core\\Topomatic.RoadSigns.Core.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\RoadMarking\\Topomatic.RoadMarking.Core\\Topomatic.RoadMarking.Core.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Extentions\\Topomatic.Proj.Runtime\\Topomatic.Proj.Runtime.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Ifc\\Topomatic.Ifc.Core\\Topomatic.Ifc.Core.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Pipes\\Topomatic.Pipes.Core\\Topomatic.Pipes.Core.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Sites\\Topomatic.Sites.Core\\Topomatic.Sites.Core.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Dwg\\Topomatic.Dwg.Controller\\Topomatic.Dwg.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Extentions\\Topomatic.Sfc.Controller\\Topomatic.Sfc.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Cartogramms\\Topomatic.Cartograms.Controller\\Topomatic.Cartograms.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Planchet\\Topomatic.Planchet.Controller\\Topomatic.Planchet.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Mockup\\Topomatic.Mockup.Controller\\Topomatic.Mockup.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Controller\\Topomatic.Alg.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Scripting\\Topomatic.Scripting\\Topomatic.Scripting.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Project.Controller\\Topomatic.Alg.Project.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Survey.Controller\\Topomatic.Alg.Survey.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Helpers.Controller\\Topomatic.Alg.Helpers.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Rail.Controller\\Topomatic.Alg.Rail.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\PlanOptimizator\\Topomatic.PlnOpt.Controller\\Topomatic.PlnOpt.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.CogoController\\Topomatic.Alg.CogoController.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Straightening.Controller\\Topomatic.Alg.Straightening.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Extentions\\Topomatic.Glg.Core\\Topomatic.Glg.Core.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Extentions\\Topomatic.Glg.Controller\\Topomatic.Glg.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\srv\\Topomatic.Srv.Controller\\Topomatic.Srv.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Extentions\\Topomatic.Maps\\Topomatic.Maps.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Turnouts\\Topomatic.Turnouts.Controller\\Topomatic.Turnouts.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Ecs\\Topomatic.Ecs.Controller\\Topomatic.Ecs.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\RoadMarking\\Topomatic.RoadMarking.Controller\\Topomatic.RoadMarking.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\RoadSigns\\Topomatic.RoadSigns.Controller\\Topomatic.RoadSigns.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Gabarit\\Topomatic.Gabarit.Controller\\Topomatic.Gabarit.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Acax\\Topomatic.Acax.Com.Controller\\Topomatic.Acax.Com.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\cadastre\\Topomatic.Cadastre.Controller\\Topomatic.Cadastre.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Lidar\\Topomatic.Lidar.Controller\\Topomatic.Lidar.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Station\\Station\\Topomatic.Station.Controller\\Topomatic.Station.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.LandAllotment.Controller\\Topomatic.Alg.LandAllotment.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Road.Crossing.Controller\\Topomatic.Alg.Road.Crossing.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Road.PlanVisibility.Controller\\Topomatic.Alg.Road.PlanVisibility.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Rsf\\Topomatic.Rsf.Controller\\Topomatic.Rsf.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Its\\Topomatic.Its.Controller\\Topomatic.Its.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Opr\\Topomatic.Opr.Controller\\Topomatic.Opr.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Alg\\Topomatic.Alg.Road.Controller\\Topomatic.Alg.Road.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Libs\\Visualization\\Topomatic.Visualization.Controller\\Topomatic.Visualization.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Tables\\Topomatic.Alg.Tables\\Topomatic.Alg.Tables.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Tables\\Topomatic.Alg.Sheets\\Topomatic.Alg.Sheets.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Tables\\Topomatic.Alg.Project.Sheets\\Topomatic.Alg.Project.Sheets.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Tables\\Topomatic.Alg.Road.Sheets\\Topomatic.Alg.Road.Sheets.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Tables\\Topomatic.Alg.Rail.Sheets\\Topomatic.Alg.Rail.Sheets.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Tables\\Topomatic.Alg.Survey.Sheets\\Topomatic.Alg.Survey.Sheets.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Tables\\Topomatic.Srv.Sheets\\Topomatic.Srv.Sheets.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Analysis\\Topomatic.Analysis.Controller\\Topomatic.Analysis.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\draught\\Topomatic.SlopeStability.Controller\\Topomatic.SlopeStability.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\draught\\Topomatic.Draught\\Topomatic.Draught.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\draught\\Topomatic.Rail.Platform\\Topomatic.Rail.Platform.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Pipes\\Topomatic.Pipes.Controller\\Topomatic.Pipes.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\CrsClearence\\Topomatic.CrcClearence.Controller\\Topomatic.CrsClearence.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\EmtDwg\\Topomatic.EmtDwg.Controller\\Topomatic.EmtDwg.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Extentions\\Topomatic.Extentions.Controller\\Topomatic.Extentions.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\edocx\\Topomatic.ExportDocumentation\\Topomatic.ExportDocumentation.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\edocx\\Topomatic.ExportDocumentation.Runtime\\Topomatic.ExportDocumentation.Runtime.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\edocx\\Topomatic.ExportDocumentation.Core\\Topomatic.ExportDocumentation.Core.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\edocx\\Topomatic.ExportDocumentation.Controller\\Topomatic.ExportDocumentation.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\cds\\Topomatic.Cds\\Topomatic.Cds.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\cds\\Topomatic.Cds.Layer\\Topomatic.Cds.Layer.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\cds\\Topomatic.Cds.Core\\Topomatic.Cds.Core.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\cds\\Topomatic.Cds.Controller\\Topomatic.Cds.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Sites\\Topomatic.Sites.Controller\\Topomatic.Sites.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Sites\\Topomatic.Brep.Controller\\Topomatic.Brep.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Culverts\\Topomatic.Culverts\\Topomatic.Culverts.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Culverts\\Topomatic.Culverts.Layers\\Topomatic.Culverts.Layers.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Culverts\\Topomatic.Culverts.Core\\Topomatic.Culverts.Core.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Culverts\\Topomatic.Culverts.Controller\\Topomatic.Culverts.Controller.csproj', args))
nargs1.items.append(NyabsParameter('Thirdparties\\Extentions\\Topomatic.Proj.Controller\\Topomatic.Proj.Controller.csproj', args))

g.init_parameters(nargs1)
g.init_parameters(NyabsCommandParameters("zipicons"))

g.append_command("build", VSBuildCommand())
g.append_command("reforder", ReferenceOrderCommand())
g.append_command("zipicons", TestZipIconsCommand())

g.run()