import asyncio
from nyabs_command import NyabsCommand
from nyabs_params import NyabsParameter, NyabsCommandParameters

class TestCreateCommand(NyabsCommand):

    def __init__(self) -> None:
        super().__init__()

    def prepare(self, args: list[NyabsParameter]) -> list[NyabsCommandParameters]:
        nargs = NyabsCommandParameters(None)
        for arg in args:
            narg = NyabsCommand(arg.value + ".tmp")
            nargs.items.append(narg)
        rslt = list[NyabsCommandParameters]()
        rslt.append(nargs)
        return rslt

    async def execute(self, args: list[NyabsParameter]) -> bool:
        for arg in args:
            process = await asyncio.create_subprocess_exec('create.bat', arg.value)
            await process.communicate()
            if (process.returncode != 0):
                self.log.error("Argument %s process executing error", arg.value)
                return False
        return True

class TestDependendCommand(NyabsCommand):

    def __init__(self) -> None:
        super().__init__()

    def prepare(self, args: list[NyabsParameter]) -> list[NyabsCommandParameters]:
        nargs = NyabsCommandParameters("accumulate")
        for arg in args:
            narg = NyabsParameter(arg.value + ".tmp", arg.args)
            nargs.items.append(narg)
        rslt = list[NyabsCommandParameters]()
        rslt.append(nargs)
        return rslt

    async def execute(self, args: list[NyabsParameter]) -> bool:
        for arg in args:
            process = await asyncio.create_subprocess_exec('create.bat', arg.value, arg.args.get("dependent", ""))
            await process.communicate()
            if (process.returncode != 0):
                self.log.error("Argument %s process executing error", arg.value)
                return False
        return True        

class TestAccumulateCommand(NyabsCommand):

    def __init__(self) -> None:
        super().__init__(accumulate=True)    

    def prepare(self, args: list[NyabsParameter]) -> list[NyabsCommandParameters]:
        nargs = NyabsCommandParameters("create")
        s = ""
        for arg in args:
            s += arg.value        
        s = s.replace('.', '_')   
        nargs.items.append(NyabsParameter(s))
        rslt = list[NyabsCommandParameters]()
        rslt.append(nargs)
        return rslt

    async def execute(self, args: list[NyabsParameter]) -> bool:
        return True   

class TestZipIconsCommand(NyabsCommand):

    def __init__(self) -> None:
        super().__init__()

    def prepare(self, args: list[NyabsParameter]) -> list[NyabsCommandParameters]:
        nargs = NyabsCommandParameters(None)    
        nargs.items.append(NyabsParameter('D:\\Topomatic\\Projects\\Out\\Bin\\Icons.zip'))
        rslt = list[NyabsCommandParameters]()
        rslt.append(nargs)
        return rslt   

    async def execute(self, args: list[NyabsParameter]) -> bool:
        self.log.info("Icons archive process started.")    
        process = await asyncio.create_subprocess_exec('C:\\Program Files\\7-Zip\\7z.exe', 'a',
            'D:\\Topomatic\\Projects\\Out\\Bin\\Icons.zip',
            'D:\\Topomatic\\Projects\\Sources\\Common\\Icons\\png\*', 
            stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE)
        stdout, stderr = await process.communicate()
        if (process.returncode != 0):
            self.log.error(str(stdout, "cp866"))    
            self.log.error(str(stderr, "cp866"))                                           
            return False
        self.log.info("Icons archive complete.")    
        return True